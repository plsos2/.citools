# .citools

A comprehensive set of tooling to get your projects build and CI ready.

Getting your project to build is now as simple as:
```bash
git submodule add https://bitbucket.org/plsos2/.citools.git
git submodule update --init --recursive
touch build
chmod a+x build
bash .citools/init dotnet
```

With the contents of the build file referencing environment variables. An example of a `build` script for a `dotnet` project is the following:

```bash
#!/bin/bash

export CAKE_VERSION="0.33.0"
export CURRENT_WORKING_DIRECTORY=$(pwd)

export SOLUTION="PropLogix.Mosaic.sln"
export SOLUTION_APPS="src/PropLogix.Mosaic.API/PropLogix.Mosaic.API.csproj"
export SOLUTION_NUGET="src/Common/**/*.csproj"
export SOLUTION_TESTS="src/tests/**/*Tests.csproj"

bash .citools/run dotnet
```

## Initialization and Build Scripts

### `init`
Initializes a project with the appropriate default skeleton files. From the root of your project after you have initial the submodule, type:
```
.citools/init <project_type>
```

## Running Project Builds

### `run`
After you have initialized your project, you can call the `run` script to execute your build.
```
.citools/run <project_type>
```

## Setup Scripts

### `aws-ecr`
Configures AWS CLI to log into the ECR registry.
```
aws-ecr [region]
```

### `setup-git`
Since most CI platforms will checkout a specific commit, which leaves the local branch in a headless state, this will perform the actions necessary to attach the correct remote repo as "origin" and checkout by branch name. Also sets the git user to an automaton account.
```
setup-git <BRANCH> <ORIGIN> <EMAIL>
```

### `setup-ssh`
Configures the remote build agent with a pre-defined SSH key from an environment variable or filename. Uses the npm package `git-ssh-key` in order to convert environment variables.
```
setup-ssh <DOMAIN> [FILEPATH]
```

### `wait-for`
Provided a hostname and a port, will wait until the port responds with a 200 OK. Useful when used in combination with services via most CI platforms.
```
wait-for <HOST> <PORT>
```
