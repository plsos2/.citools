#!/bin/bash

AWSROOT=$HOME/.aws

declare -a ENVIRONMENTS
ENVIRONMENTS=(dev stage prod)

if [[ ! -d "$AWSROOT" || ! -f "$AWSROOT/config" ]]; then
  mkdir -p $AWSROOT
  echo "** Writing Config: $AWSROOT/config"
  echo "[default]" >$AWSROOT/config
  echo "region = us-east-1" >>$AWSROOT/config
fi

if [[ ! -d "$AWSROOT" || ! -f "$AWSROOT/credentials" ]]; then
  AWSCREDS=$AWSROOT/credentials
  echo "** Writing Credential: $AWSCREDS"
  echo "[proplogix]" >>$AWSCREDS
  echo "aws_access_key_id = $PRIMARY_ACCESS_KEY" >>$AWSCREDS
  echo "aws_secret_access_key = $PRIMARY_SECRET_KEY" >>$AWSCREDS

  echo "** Reading secrets from AWS..."
  SECRETS=$(aws secretsmanager --profile proplogix --region us-east-1 \
    get-secret-value --secret-id terraform-secrets \
    --query 'SecretString' --output text)

  EXIT_CODE=$?
  if [ $EXIT_CODE != 0 ]; then
    echo $EXIT_CODE
    exit $EXIT_CODE
  fi

  for ENVIRONMENT in ${ENVIRONMENTS[@]}; do
    ACCESS_KEY_NAME=$(echo PLX_ACCESS_KEY_$ENVIRONMENT | tr a-z A-Z)
    SECRET_KEY_NAME=$(echo PLX_SECRET_KEY_$ENVIRONMENT | tr a-z A-Z)
    ACCESS_KEY=$(echo $SECRETS | jq .$ACCESS_KEY_NAME | sed -e 's/^"//' -e 's/"$//')
    SECRET_KEY=$(echo $SECRETS | jq .$SECRET_KEY_NAME | sed -e 's/^"//' -e 's/"$//')
    echo "** Writing Credential: proplogix-$ENVIRONMENT -> $ACCESS_KEY_NAME"
    echo "[proplogix-$ENVIRONMENT]" >>$AWSCREDS
    echo "aws_access_key_id = $ACCESS_KEY" >>$AWSCREDS
    echo "aws_secret_access_key = $SECRET_KEY" >>$AWSCREDS
  done
fi
